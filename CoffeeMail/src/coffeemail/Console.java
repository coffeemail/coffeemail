package coffeemail;

import java.util.Scanner;

import coffeemail.modul.MailModul;
import coffeemail.modul.event.console.CommandExecuteEvent;

public class Console implements Runnable {

	private boolean running = true;

	@Override
	public void run() {
		Scanner scan = new Scanner(System.in);
		while (scan.hasNext() && running) {
			String line = scan.nextLine();

			if (CoffeeMail.isIsdebug()) {
				if (line.equalsIgnoreCase("stop")) {
					CoffeeMail.shutdown(true);
				} else if (line.equalsIgnoreCase("exit")) {
					CoffeeMail.setDebug(false);
				}
			} else {
				if (line.equalsIgnoreCase("stop")) {
					CoffeeMail.shutdown(true);
				} else if (line.equalsIgnoreCase("update")) {
					CoffeeMail.update();
					CoffeeMail.shutdown(false);
				} else if (line.equalsIgnoreCase("delete")) {
					CoffeeMail.delete();
					CoffeeMail.shutdown(false);
				} else if (line.equalsIgnoreCase("debug")) {
					CoffeeMail.setDebug(true);
				} else if (line.equalsIgnoreCase("module")) {
					int count = MailServer.getModulManager().getModuls().size();
					CoffeeMail.log("=== Loaded Modules [" + count + "] ===");
					if (count == 0) {
						CoffeeMail.log("There are 0 loaded Modules");
					}
					for (MailModul m : MailServer.getModulManager().getModuls()) {
						CoffeeMail.log(m.getName() + "[" + m.getVersion()
								+ "] by " + m.getAuthor());
					}
					CoffeeMail.log("==========================");
				} else {
					if (!new CommandExecuteEvent(line).call().isExist()) {
						CoffeeMail.log("There is no Command like '" + line
								+ "'");
					}
				}
			}
		}
		scan.close();
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}
}
