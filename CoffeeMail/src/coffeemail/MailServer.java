package coffeemail;

import coffeemail.config.ConfigManager;
import coffeemail.modul.ModulManager;
import coffeemail.smtp.receiver.MailReceiveManager;
import coffeemail.smtp.sender.MailSendManager;

public class MailServer {

	private static MailSendManager msm;
	private static MailReceiveManager mrm;
	private static ModulManager mm;
	private static ConfigManager cm;
	private static Console c;

	public static void start() {
		if (!CoffeeMail.newVersionavailable().isEmpty()) {
			System.out.println("##########################################");
			System.out.println("A new Version of " + CoffeeMail.name
					+ " is available!");
			System.out.println("Update with \"update\" or restart");
			System.out.println("with \"update\" as parameter!");
			System.out.println("##########################################");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		mm = new ModulManager();
		c = new Console();
		new Thread(c).start();
		cm = new ConfigManager();
		CoffeeMail.log("Start MailSendManager-Service");
		msm = new MailSendManager();

		CoffeeMail.log("Start MailReceiveManager-Service");
		mrm = new MailReceiveManager();
		new Thread(mrm).start();

		CoffeeMail.log("Start ModulManager-Service");
		mm.loadModuls();

		CoffeeMail.log("Done!");
	}

	public static MailSendManager getMailSendManager() {
		return msm;
	}

	public static MailReceiveManager getMailReceiveManager() {
		return mrm;
	}

	public static ModulManager getModulManager() {
		return mm;
	}

	public static ConfigManager getConfigManager() {
		return cm;
	}

	public static Console getConsole() {
		return c;
	}
}
