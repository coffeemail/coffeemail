package coffeemail.mail.content;

public class MailContent {

	public ContentType type;
	public String content;

	// public abstract String[] write();

	public MailContent(String base64, ContentType type) {
		this.content = base64;
		this.type = type;
	}

	public enum ContentType {
		HTML(""), TEXT(""), NULL("");

		private String type;

		ContentType(String type) {
			this.type = type;
		}

		public String setType(String type) {
			this.type = type;
			return type;
		}

		public String typeToString() {
			return type;
		}
	}
}
