package coffeemail.mail.file;

import coffeemail.mail.Address;
import coffeemail.mail.Mail;

public interface FileStorage {

	public void saveMail(Mail m);

	public void loadMailbyID(Mail m);

	public void loadMailreceiver(Mail m);

	public void getUserbyEmail(Address a);

	// getUserbyName

}