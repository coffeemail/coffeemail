package coffeemail.modul;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import coffeemail.CoffeeMail;
import coffeemail.modul.annotation.ModulAuthor;
import coffeemail.modul.annotation.ModulConfig;
import coffeemail.modul.annotation.ModulName;
import coffeemail.modul.annotation.ModulVersion;

public class MailModul {

	private Modul modul;
	private String name;
	private String author;
	private String version;
	private boolean start = false;

	public MailModul(Modul modul) {
		this.modul = modul;

		try {
			Method m = modul.getClass().getMethod("load");
			Class<? extends Modul> c = modul.getClass();

			if (m != null) {
				String modulname = "Unknown";
				String modulauthor = "Unknown";
				String moduleversion = "1.0";
				if (c.isAnnotationPresent(ModulName.class)) {
					Annotation annotation = c.getAnnotation(ModulName.class);
					ModulName mn = (ModulName) annotation;
					modulname = mn.modulname();
				}
				if (c.isAnnotationPresent(ModulAuthor.class)) {
					Annotation annotation = c.getAnnotation(ModulAuthor.class);
					ModulAuthor mn = (ModulAuthor) annotation;
					modulauthor = mn.modulauthor();
				}
				if (c.isAnnotationPresent(ModulVersion.class)) {
					Annotation annotation = c.getAnnotation(ModulVersion.class);
					ModulVersion mn = (ModulVersion) annotation;
					moduleversion = mn.modulversion();
				}

				this.name = modulname;
				this.author = modulauthor;
				this.version = moduleversion;
				this.start = true;
			}
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
	}

	public void init() {
		if (this.start) {
			CoffeeMail
					.log("Loading " + name + "[" + version + "] by " + author);
			config();
			load();
		} else {
			CoffeeMail.log("Unable to load " + name + "[" + version + "]");
		}
	}

	public void config() {
		File moduledir = new File(CoffeeMail.defaultfolder, "module/"
				+ getName());
		if (!moduledir.exists()) {
			moduledir.mkdir();
		}
		File config = new File(CoffeeMail.defaultfolder, "module/" + getName()
				+ "/config");
		if (!config.exists()) {
			try {
				config.createNewFile();
				saveConfig(config);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			loadConfig(config);
		}
	}

	public void saveConfig(File config) {
		Field[] fields = getModul().getClass().getFields();
		String json = "{";
		for (Field field : fields) {
			if (field.isAnnotationPresent(ModulConfig.class)) {
				try {
					json += "\"" + field.getName() + "\":"
							+ new Gson().toJson(field.get(getModul())) + ",";
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}

		}
		json = json.substring(0, json.length() - 1) + "}";
		if (fields.length == 0) {
			json = "{}";
		}
		try {
			PrintWriter writer = new PrintWriter(config, "UTF-8");
			writer.write(json);
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	public void loadConfig(File config) {
		String json = "";
		try {
			Scanner scan = new Scanner(config);
			while (scan.hasNext()) {
				json += scan.next();
			}
			scan.close();
		} catch (FileNotFoundException e) {
		}
		JsonObject jobj = new Gson().fromJson(json, JsonObject.class);
		Set<Entry<String, JsonElement>> set = jobj.entrySet();

		Iterator<Entry<String, JsonElement>> i = set.iterator();

		while (i.hasNext()) {
			Entry<String, JsonElement> ex = i.next();
			try {
				Field f = getModul().getClass().getField(ex.getKey());
				if (f != null) {
					Class<?> typeclass = f.getType();
					if (typeclass.equals(String.class)) {
						f.set(getModul(), ex.getValue().getAsString());
					} else if (typeclass.equals(Integer.class)
							|| typeclass.equals(int.class)) {
						f.setInt(getModul(), ex.getValue().getAsInt());
					} else if (typeclass.equals(Character.class)
							|| typeclass.equals(char.class)) {
						f.setChar(getModul(), ex.getValue().getAsCharacter());
					} else if (typeclass.equals(Boolean.class)
							|| typeclass.equals(boolean.class)) {
						f.setBoolean(getModul(), ex.getValue().getAsBoolean());
					} else if (typeclass.equals(Double.class)
							|| typeclass.equals(double.class)) {
						f.setDouble(getModul(), ex.getValue().getAsDouble());
					}
				}
			} catch (IllegalArgumentException | IllegalAccessException
					| NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			}
		}
	}

	public void load() {
		getModul().load();
	}

	public void unload() {
		getModul().unload();
	}

	public Modul getModul() {
		return modul;
	}

	public String getName() {
		return name;
	}

	public String getAuthor() {
		return author;
	}

	public String getVersion() {
		return version;
	}

}
