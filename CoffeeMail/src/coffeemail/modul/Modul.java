package coffeemail.modul;

import coffeemail.CoffeeMail;
import coffeemail.MailServer;
import coffeemail.modul.event.EventHandler;

public class Modul {

	public void load() {

	}

	public void unload() {

	}

	public ModulManager getModulManager() {
		return MailServer.getModulManager();
	}

	public EventHandler getEventHandler() {
		return MailServer.getModulManager().getEventHandler();
	}

	public void log(String message) {
		CoffeeMail.log(message);
	}

	public void debug(String message) {
		CoffeeMail.debug(message);
	}

	public void error(Exception exception) {
		CoffeeMail.error(exception);
	}
}
