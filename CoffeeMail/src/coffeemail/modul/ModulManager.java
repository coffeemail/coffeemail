package coffeemail.modul;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import coffeemail.CoffeeMail;
import coffeemail.MailServer;
import coffeemail.modul.annotation.ModulMain;
import coffeemail.modul.event.EventHandler;

public class ModulManager {

	private ArrayList<MailModul> moduls = new ArrayList<MailModul>();
	private EventHandler handler = new EventHandler();

	public void loadModuls() {
		try {
			File file = new File(new File(MailServer.class
					.getProtectionDomain().getCodeSource().getLocation()
					.toURI().getPath()).getParentFile(), "module");
			for (File f : file.listFiles()) {
				if (f.isFile() && f.getName().endsWith(".cmm")) {

					CoffeeMail.debug("Found "
							+ f.getName()
									.substring(0, f.getName().length() - 4)
							+ "-Module");
					loadModul(f);
				}
			}
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	// Taken from stackoverflow:
	// http://stackoverflow.com/questions/26949333/load-a-class-in-a-jar-just-with-his-name

	public void loadModul(File file) {
		try {
			boolean loaded = false;
			JarFile jarFile = new JarFile(file);
			Enumeration<JarEntry> e = jarFile.entries();

			URL[] urls = { new URL("jar:file:" + file.getPath() + "!/") };
			URLClassLoader cl = URLClassLoader.newInstance(urls);

			while (e.hasMoreElements()) {
				JarEntry je = (JarEntry) e.nextElement();
				if (je.isDirectory() || !je.getName().endsWith(".class")) {
					continue;
				}
				String className = je.getName().substring(0,
						je.getName().length() - 6);
				className = className.replace('/', '.');
				Class<?> c = cl.loadClass(className);

				try {
					// if (c.isInstance(Modul.class)) {
					if (c.isAnnotationPresent(ModulMain.class)) {

						Method m = c.getMethod("load");
						if (m != null) {
							Modul modul = (Modul) c.newInstance();
							MailModul mm = new MailModul(modul);
							moduls.add(mm);
							mm.init();
							loaded = true;
						}
					} else {
					}
					// }
				} catch (NoSuchMethodException ex) {
				}
			}
			jarFile.close();
			if (!loaded) {
				CoffeeMail.log("No Main-Class found!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<MailModul> getModuls() {
		return moduls;
	}

	public EventHandler getEventHandler() {
		return handler;
	}
}
