package coffeemail.modul.event;

public class Event {

	private boolean isCancelled = false;

	public boolean isCancelled() {
		return isCancelled;
	}

	public void setCancelled(boolean isCancelled) {
		this.isCancelled = isCancelled;
	}
}
