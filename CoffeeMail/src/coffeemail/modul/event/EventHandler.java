package coffeemail.modul.event;

import java.util.ArrayList;
import java.util.List;

import coffeemail.CoffeeMail;
import coffeemail.modul.event.console.CommandExecuteEvent;
import coffeemail.modul.event.mail.MailReceiveEvent;
import coffeemail.modul.event.mail.MailSendEvent;
import coffeemail.modul.event.mail.PreMailReceiveEvent;
import coffeemail.modul.event.mail.PreMailSendEvent;

public class EventHandler {

	private List<Listener> listeners = new ArrayList<Listener>();

	public void addListener(Listener listener) {
		listeners.add(listener);
	}

	public void removeListener(Listener listener) {
		listeners.remove(listener);
	}

	public void sendMailEvent(MailSendEvent e) {
		for (Listener listener : listeners) {
			try {
				listener.sendMailEvent(e);
			} catch (Exception exception) {
				CoffeeMail.error(exception);
			}
		}
	}

	public void presendMailEvent(PreMailSendEvent e) {
		for (Listener listener : listeners) {
			try {
				listener.presendMailEvent(e);
			} catch (Exception exception) {
				CoffeeMail.error(exception);
			}
		}
	}

	public void receiveMailEvent(MailReceiveEvent e) {
		for (Listener listener : listeners) {
			try {
				listener.receiveMailEvent(e);
			} catch (Exception exception) {
				CoffeeMail.error(exception);
			}
		}
	}

	public void prereceiveMailEvent(PreMailReceiveEvent e) {
		for (Listener listener : listeners) {
			try {
				listener.prereceiveMailEvent(e);
			} catch (Exception exception) {
				CoffeeMail.error(exception);
			}
		}
	}

	public void executeCommand(CommandExecuteEvent e) {
		for (Listener listener : listeners) {
			try {
				listener.executeCommand(e);
			} catch (Exception exception) {
				CoffeeMail.error(exception);
			}
		}
	}

	public void removeAllListener() {
		listeners.clear();
	}

	public List<Listener> getListeners() {
		return listeners;
	}

}
