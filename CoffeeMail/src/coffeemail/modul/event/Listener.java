package coffeemail.modul.event;

import coffeemail.modul.event.console.CommandExecuteEvent;
import coffeemail.modul.event.mail.MailReceiveEvent;
import coffeemail.modul.event.mail.MailSendEvent;
import coffeemail.modul.event.mail.PreMailReceiveEvent;
import coffeemail.modul.event.mail.PreMailSendEvent;

public interface Listener {

	public void sendMailEvent(MailSendEvent e);

	public void presendMailEvent(PreMailSendEvent e);

	public void receiveMailEvent(MailReceiveEvent e);

	public void prereceiveMailEvent(PreMailReceiveEvent e);

	public void executeCommand(CommandExecuteEvent e);
}
