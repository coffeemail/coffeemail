package coffeemail.modul.event.console;

import coffeemail.MailServer;
import coffeemail.modul.event.Event;

public class CommandExecuteEvent extends Event {

	private String command;
	private boolean exist = false;

	public CommandExecuteEvent(String command) {
		this.command = command;
	}

	public String getCommand() {
		return command;
	}

	public boolean isExist() {
		return exist;
	}

	public void setExist(boolean exist) {
		this.exist = exist;
	}

	public CommandExecuteEvent call() {
		MailServer.getModulManager().getEventHandler().executeCommand(this);
		return this;
	}
}
