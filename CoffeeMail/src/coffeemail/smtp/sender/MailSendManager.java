package coffeemail.smtp.sender;

import coffeemail.mail.Address;
import coffeemail.mail.Mail;

public class MailSendManager {

	public MailSendManager() {

	}

	public boolean send(Mail mail) {
		try {
			new Thread(new MailSendClient(mail, mail.getReceiver())).start();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		for (Address address : mail.getCCReceiver()) {
			try {
				new Thread(new MailSendClient(mail, address)).start();
			} catch (RuntimeException e) {
				e.printStackTrace();
			}
		}
		for (Address address : mail.getBCCReceiver()) {
			try {
				new Thread(new MailSendClient(mail, address)).start();
			} catch (RuntimeException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
}
