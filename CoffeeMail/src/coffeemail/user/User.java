package coffeemail.user;

import coffeemail.mail.Address;
import coffeemail.util.Util;

public class User {

	private String username;
	private String hashedpassword;
	private boolean admin;
	private Address[] emails;

	public String getUsername() {
		return username;
	}

	public String getHashedpassword() {
		return hashedpassword;
	}

	public boolean isAdmin() {
		return admin;
	}

	public Address[] getEmails() {
		return emails;
	}

	public boolean validLogin(String password) {
		return Util.getHash(password).equals(getHashedpassword());
	}

}
