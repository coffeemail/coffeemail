package coffeemail.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import coffeemail.MailServer;

public class Util {

	public static String generateRandomPassword() {
		SecureRandom sr = new SecureRandom();
		String letters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ234567890123456789";

		String pw = "";
		for (int i = 0; i < 20; i++) {
			int index = (int) (sr.nextDouble() * letters.length());
			pw += letters.substring(index, index + 1);
		}
		return pw;
	}

	public static String getHash(String password) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return new String(md.digest((password + MailServer.getConfigManager()
				.getConfig().getSalt()).getBytes()));
	}

}
